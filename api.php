<?php
use Models\Category;
use Models\Message;

$route->group('/api', function()
{

$this->get('categories', function(){
    $c = Category::get();
    $categories = [];
    foreach ($c as $value) {
        $categories[] = array(
            'id' => $value->id,
            'name' => $value->name,
            'count' => number_format(Category::find($value->id)->messages->count())
        );
    }
    // print_r($categories);
    // $c  = Category::find(1)->messages->count();
    // print_r($c);
    send (json_encode($categories));
});

$this->get('messages/{category}?', function($category){
   $query = app('request')->query;
   $offset = 0;
   $page = 1;
   $page_result = 20;
   if(!empty($query['page'])){
       $page = intval($query['page']);
   }
   if($page > 1){
    $offset = ($page - 1) * $page_result;
   }
   $msgs = Message::skip($offset)->take($page_result);
   if(isset($category)){
    $msgs = $msgs->where('category_id',$category);
   }
   send ($msgs->orderBy('created_at', 'desc')->get()->toJson());

});

});

function send($json){
    header('Content-Type: application/json');
    echo $json;
}