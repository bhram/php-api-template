<?php

namespace Models;

use \Illuminate\Database\Eloquent\Model;

class Message extends Model {

    protected $table = 'messages';

    public function category(){
//        Cate
       return $this->belongsTo('Models\Category');
    }
}